//
// Created by Daniele Giuliani on 6/21/22.
//

#ifndef DISTROINFOS_COMMON_H
#define DISTROINFOS_COMMON_H
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <ctype.h>
#include <string.h>
#include "../../cJSON/cJSON.h"

char *read_file(const char *file_name);
int parse_numbers(const char *str);
#endif //DISTROINFOS_COMMON_H
