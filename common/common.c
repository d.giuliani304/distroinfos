//
// Created by Daniele Giuliani on 6/21/22.
//

#include "include/common.h"

char* read_file(const char *filename) {
    FILE *file = NULL;
    long length = 0;
    char *content = NULL;
    size_t read_chars = 0;
    // open in read binary mode
    file = fopen(filename, "rt");
    if (file == NULL) {
        goto cleanup;
    }
    // get the length
    if (fseek(file, 0, SEEK_END) != 0) {
        goto cleanup;
    }
    length = ftell(file);
    if (length < 0) {
        goto cleanup;
    }
    if (fseek(file, 0, SEEK_SET) != 0) {
        goto cleanup;
    }
    // allocate content buffer
    content = (char*)malloc((size_t)length + sizeof(""));
    if (content == NULL) {
        goto cleanup;
    }
    // read the file into memory
    read_chars = fread(content, sizeof(char), (size_t)length, file);
    if ((long)read_chars != length) {
        free(content);
        content = NULL;
        goto cleanup;

    }
    content[read_chars] = '\0';
    cleanup:
        if (file != NULL) {
            fclose(file);
        }
        return content;
}

int parse_numbers(const char *str) {
    char result[strlen(str)];
    int position = 0;
    size_t i = 0;
    while (str[i] != '\0') {
        if (isdigit(str[i])) {
            char s;
            s = str[i];
            strcpy(&result[position], &s);
            position++;
        }
        i++;
    }
    return strtol(result, NULL, 10);
}