//
// Created by Daniele Giuliani on 2/17/21.
//
#define CONSOLE_IMPORT
#include "include/console.h"

/**
 * print
 * @params char *message
 * @brief @ a string printError with a param message RED color
 * @return void
 *
 */
void printError(char *message)
{
    printf(AC_RED"%s\n"AC_RESET, message);
}

/**
 * @params char *message
 * @brief print a string Success with a GREEN color
 * @return void
 *
 */
void printSuccess(char *message)
{
    printf(AC_GREEN"%s\n"AC_RESET, message);
}

/**
 * @params char *message
 * @brief print a string Warning with a YELLOW color
 * @return void
 *
 */
void printWarning(char *message)
{
    printf(AC_YELLOW"%s\n"AC_RESET, message);
}

/**
 * @params char *message
 * @brief print a string Primary with a CYAN color
 * @return void
 *
 */
void printPrimary(char *message)
{
    printf(AC_CYAN"%s\n"AC_RESET, message);
}

/**
 * @params char *message
 * @brief print a string Strong with a BLUE color
 * @return void
 *
 */
void printStrong(char *message)
{
    printf(AC_BLUE"%s\n"AC_RESET, message);
}

/**
 * @params char *message
 * @brief print a string Info with a MAGENTA color
 * @return void
 *
 */
void printInfo(char *message)
{
    printf(AC_MAGENTA"%s\n"AC_RESET, message);
}