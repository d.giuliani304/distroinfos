//
// Created by Daniele Giuliani on 2/13/22.
//

#ifndef DISTROINFOS_INCLUDE_H
#define DISTROINFOS_INCLUDE_H

#include "../console/include/console.h"
#include "../entities/include/entities.h"

#ifdef DISTROINFOS_IMPORT
#define EXTERN
#else
#define EXTERN extern
#endif

#undef DISTROINFOS_IMPORT
#undef EXTERN
#endif //DISTROINFOS_INCLUDE_H
