//
// Created by Daniele Giuliani on 6/13/22.
//

#ifndef DISTROINFOS_DEBUG_H
#define DISTROINFOS_DEBUG_H
#ifdef DEBUG
#define Debug if(1)
#define Release if(0)
#else
#define Debug if(0)
#define Release if(1)
#endif
#endif //DISTROINFOS_DEBUG_H
