#include "include/include.h"

int initSDL()
{
    // CREATE WINDOW SURFACE AND RENDERING STUFF
    // RETURN TRUE
}

int initApp()
{
    // INITIALIZA SDL2
    // LOAD APPLICATION DATA
    // CREATE MAIN PROCESS
    // RETURN TRUE
}


int main() {

    StringClass *s = new(String, "name");
    delete(s);
    ShapeClass *sh = new(
        Shape,
        new(Vec3, 10.0f, 10.0f, 10.0f),
        new(Vec4, 10.0f, 10.0f, 10.0f, 10.0f)
    );
    delete(sh);
    Point2Class *p = new(Point2, new(Vec2, 10.0f, 10.0f));
    delete(p);
    Point3Class *p1 = new(Point3, new(Vec3, 10.0f, 10.0f, 10.0f));
    delete(p1);

    WindowClass *splash = new(
        Window,
        "splash window",
        CENTER,
        800,
        600,
        SPLASH
    );
    splash->prepareRender(splash);
    ProgressClass *progress = new(Progress, splash);
    if(progress->setup(progress, "../templates/ui/progress_bar/splash.json")){
        progress->draw(progress);
        splash->render(splash);
        for (int x = 1; x <= 10000; x++) {
            progress->progress = x * 0.0001;
            splash->prepareRender(splash);
            progress->draw(progress);
            splash->render(splash);
        }
    }else{
        splash->render(splash);
    }
    Debug printSuccess(splash->name->text);
    Debug printSuccess(splash->uuid->uuid);
    delete(progress);
    delete(splash);

    WindowClass *main = new(
        Window,
        "main window",
        CENTER,
        1024,
        768,
        MAIN
    );
    Debug printSuccess(main->name->text);
    Debug printSuccess(main->uuid->uuid);
    delete(main);
    Debug printError("Hello World!");
    Debug printSuccess("Hello World!");
    Debug printWarning("Hello World!");
    Debug printPrimary("Hello World!");
    Debug printStrong("Hello World!");
    Debug printInfo("Hello World!");
    return 0;
}
