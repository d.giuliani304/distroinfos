var struct_w_i_n_d_o_w =
[
    [ "class", "struct_w_i_n_d_o_w.html#a0399eb78125961fc22085d4aac6b0d15", null ],
    [ "is_main_window", "struct_w_i_n_d_o_w.html#a525d7765f214d3564dd0646d4459fee6", null ],
    [ "name", "struct_w_i_n_d_o_w.html#ac20922fac488c617dcd7fa781aaef2db", null ],
    [ "prepareRender", "struct_w_i_n_d_o_w.html#ab5e3aa2ee28fe5a624f1c45095fd2b51", null ],
    [ "render", "struct_w_i_n_d_o_w.html#af8ba408155d09b4ff60666c146839ca7", null ],
    [ "renderer", "struct_w_i_n_d_o_w.html#a966da7a60c4ea3ba301e26ccc5efe452", null ],
    [ "splash", "struct_w_i_n_d_o_w.html#ae54efda219ff233e2e9091a7300cb33e", null ],
    [ "surface", "struct_w_i_n_d_o_w.html#a2f5cac12e913bcfcff660305bf88dd3b", null ],
    [ "texture", "struct_w_i_n_d_o_w.html#a859b8efbf9abe8e82757ee5c75a0c97c", null ],
    [ "uuid", "struct_w_i_n_d_o_w.html#aa5c30ec3e3fc372af365f299e7a6abde", null ],
    [ "window", "struct_w_i_n_d_o_w.html#aaa8e409e04dcf575ef63fd5fb3db06f9", null ]
];