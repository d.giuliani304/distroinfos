var annotated_dup =
[
    [ "CLASS", "struct_c_l_a_s_s.html", "struct_c_l_a_s_s" ],
    [ "POINT2", "struct_p_o_i_n_t2.html", "struct_p_o_i_n_t2" ],
    [ "POINT3", "struct_p_o_i_n_t3.html", "struct_p_o_i_n_t3" ],
    [ "PROGRESS", "struct_p_r_o_g_r_e_s_s.html", "struct_p_r_o_g_r_e_s_s" ],
    [ "SHAPE", "struct_s_h_a_p_e.html", "struct_s_h_a_p_e" ],
    [ "STRING", "struct_s_t_r_i_n_g.html", "struct_s_t_r_i_n_g" ],
    [ "UUID", "struct_u_u_i_d.html", "struct_u_u_i_d" ],
    [ "VEC", "struct_v_e_c.html", "struct_v_e_c" ],
    [ "VEC2", "struct_v_e_c2.html", "struct_v_e_c2" ],
    [ "VEC3", "struct_v_e_c3.html", "struct_v_e_c3" ],
    [ "VEC4", "struct_v_e_c4.html", "struct_v_e_c4" ],
    [ "VECLIST", "struct_v_e_c_l_i_s_t.html", "struct_v_e_c_l_i_s_t" ],
    [ "WINDOW", "struct_w_i_n_d_o_w.html", "struct_w_i_n_d_o_w" ]
];