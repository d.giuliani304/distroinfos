var indexSectionsWithContent =
{
  0: "abcdefhimnoprstuvwxyz",
  1: "cpsuvw",
  2: "cdeimpsuvw",
  3: "cdfimnprs",
  4: "bcdehimnoprstuvwxyz",
  5: "cpsuvw",
  6: "w",
  7: "cmsu",
  8: "acdeinor"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros"
};

