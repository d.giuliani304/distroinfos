var struct_p_r_o_g_r_e_s_s =
[
    [ "class", "struct_p_r_o_g_r_e_s_s.html#a0399eb78125961fc22085d4aac6b0d15", null ],
    [ "color", "struct_p_r_o_g_r_e_s_s.html#abff8258b539902f219c0b34c403d0f76", null ],
    [ "draw", "struct_p_r_o_g_r_e_s_s.html#a02dd5bde961c054eabc8c7122b56b744", null ],
    [ "has_progress", "struct_p_r_o_g_r_e_s_s.html#a6fcd374bc227866ee35dc07b6a6d20c4", null ],
    [ "margin", "struct_p_r_o_g_r_e_s_s.html#af115413ba420d8fe35c15ef27f90f7ad", null ],
    [ "next", "struct_p_r_o_g_r_e_s_s.html#a3d60e8e01eae477b5d8af7e762f5d8b5", null ],
    [ "opacity", "struct_p_r_o_g_r_e_s_s.html#a6f4dca6b48529c4d3dade0e8d46f4e43", null ],
    [ "position", "struct_p_r_o_g_r_e_s_s.html#ade86a07d1b8e910ce1af918c1e5a8683", null ],
    [ "prev", "struct_p_r_o_g_r_e_s_s.html#a4718f355fd6ec3d4ab269d7055c8f855", null ],
    [ "progress", "struct_p_r_o_g_r_e_s_s.html#ac7abb4766cd3f65c31f56279d7decff8", null ],
    [ "rect", "struct_p_r_o_g_r_e_s_s.html#a55aefd071649ac9dd8133e2d8a52d11f", null ],
    [ "setup", "struct_p_r_o_g_r_e_s_s.html#a09bed13b66a4a5118ec654e837638e9b", null ],
    [ "size", "struct_p_r_o_g_r_e_s_s.html#a4e90d4c16be129dc83da1fe55ea04379", null ],
    [ "target_window", "struct_p_r_o_g_r_e_s_s.html#a911625119c23f07f008c2b6e0b9748d5", null ]
];