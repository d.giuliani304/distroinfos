//
// Created by Daniele Giuliani on 2/21/22.
//
#include "include/shape.h"
#include "../include/entities.h"

Vec3Class *Shape_Position(ShapeClass * self, double x, double y, double z) {
    self->position->x = x;
    self->position->y = y;
    self->position->z = z;
    return self->position;
}

Vec4Class *Shape_Rotation(ShapeClass *self, double x, double y, double z, double w) {
    self->rotation->x = x;
    self->rotation->y = y;
    self->rotation->z = z;
    self->rotation->w = w;
    return self->rotation;
}

static void *Shape_ctor(void *_self, va_list *app)
{
    ShapeClass *self =_self;
    self->position = va_arg(*app, Vec3Class *);
    assert(self->position);
    self->rotation = va_arg(*app, Vec4Class *);
    assert(self->rotation);
    self->setPosition = Shape_Position;
    self->setRotation = Shape_Rotation;
    return self;
}

static void *Shape_dtor(void *_self)
{
    ShapeClass *self = _self;
    delete(self->position);
    delete(self->rotation);
    free(_self);
    return self;
}

static void *Shape_clone(const void *_self)
{
    const ShapeClass *self = _self;
    return new(Shape, self->position, self->rotation);
}

static int Shape_equal(const void *_self, const void *_b)
{
    const ShapeClass *self = _self;
    const ShapeClass *b = _b;
    if (self is b)
    {
        return 0;
    }
    if (not b or b->class isnt Shape)
    {
        return 1;
    }
    return self->position is b->position and self->rotation is b->rotation;
}

static const Class shape = {
    sizeof(ShapeClass),
    Shape_ctor,
    Shape_dtor,
    Shape_clone,
    Shape_equal
};

const void * Shape = &shape;
