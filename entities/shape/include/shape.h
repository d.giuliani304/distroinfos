//
// Created by Daniele Giuliani on 2/21/22.
//

#ifndef DISTROINFOS_SHAPE_H
#define DISTROINFOS_SHAPE_H

#include "../../vectors/include/vectors.h"

#ifdef SHAPE_IMPORT
#define EXTERN
#else
#define EXTERN extern
#endif

typedef struct SHAPE ShapeClass;

struct SHAPE {
    const void *class;	/* must be first */
    Vec3Class *position;
    Vec4Class *rotation;
    Vec3Class *(*setPosition) (ShapeClass* self, double x, double y, double z);
    Vec4Class *(*setRotation) (ShapeClass* self, double x, double y, double z, double w);
};

/**
 * @brief create a shape passing a Vec3 Position, Vec4 Rotation and a Mesh
 * @param Vec3 Position
 * @param Vec4 Rotation
 * @param Mesh[Vec3] mesh
 * @example
 * ShapeClass *s = new(Shape, new(Vec3, ...), new(Vec4, ...));\n
 */
EXTERN const void * Shape;

#undef SHAPE_IMPORT
#undef EXTERN

#endif //DISTROINFOS_SHAPE_H
