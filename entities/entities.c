//
// Created by Daniele Giuliani on 2/13/22.
//
#include "include/entities.h"

void *new(const void *_class, ...)
{
    const Class *class = _class;
    void * p = calloc(1, class->size);
    assert(p);
    * (const Class **) p = class;
    if (class->ctor)
    {
        va_list ap;
        va_start(ap, _class);
        p = class->ctor(p, & ap);
        va_end(ap);
    }
    return p;
}

void delete(void *self)
{
    const Class ** cp = self;
    if (self and *cp and (*cp)->dtor)
    {
        self = (*cp)->dtor(self);
    }
}

void *clone(const void *self)
{
    const Class *const *cp = self;
    assert(self and *cp and (*cp)->clone);
    return (*cp)->clone(self);
}

int differ(const void *self, const void *b)
{
    const Class *const *cp = self;
    assert(self and *cp and (*cp)->differ);
    return (*cp)->differ(self, b);
}

size_t sizeOf(const void *self)
{
    const Class *const *cp = self;
    assert(self and *cp);
    return (*cp)->size;
}
