//
// Created by Daniele Giuliani on 2/21/22.
//

#include "include/vectors.h"
//
//static void *VecList_ctor(void *_self, va_list *app)
//{
//    VecListClass *self = _self;
//    // self->x = va_arg(*app, double);
//    // self->y = va_arg(*app, double);
//    // self->z = va_arg(*app, double);
//    // self->w = va_arg(*app, double);
//    return self;
//}
//
//static void *Vec_ctor(void *_self, va_list *app)
//{
//    VecClass *self = _self;
//    // self->x = va_arg(*app, double);
//    // self->y = va_arg(*app, double);
//    // self->z = va_arg(*app, double);
//    // self->w = va_arg(*app, double);
//    return self;
//}
//
//static void *VecList_dtor(void *_self)
//{
//    VecListClass *self = _self;
//    // self->x = 0;
//    // self->y = 0;
//    // self->z = 0;
//    // self->w = 0;
//    free(_self);
//    return self;
//}
//
//static void *VecList_clone(const void *_self)
//{
//    const VecListClass *self = _self;
//    return new(VecList, self->x, self->y, self->z, self->w);
//}
//
//static void *Vec_clone(const void *_self)
//{
//    const VecClass *self = _self;
//    return new(Vec, self->x, self->y, self->z, self->w);
//}
//
//static int VecList_equal(const void *_self, const void *_b)
//{
//    const VecListClass *self = _self;
//    const VecListClass *b = _b;
//    if (self is b)
//    {
//        return 0;
//    }
//    if (not b or b->class isnt VecList)
//    {
//        return 1;
//    }
//    return self->size is b->size and self->start is b->start and self->end is b->end;
//}
//
//static const Class vec_list = {
//        sizeof(VecListClass),
//        VecList_ctor,
//        VecList_dtor,
//        VecList_clone,
//        VecList_equal
//};
//
//const void * VecList = &vec_list;
//
//static const Class vec = {
//        sizeof(VecClass),
//        Vec_ctor,
//        Vec_dtor,
//        Vec_clone,
//        Vec_equal
//};
//
//const void * Vec = &vec;
