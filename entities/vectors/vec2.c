//
// Created by Daniele Giuliani on 2/21/22.
//

#include "include/vec2.h"
#include "../include/entities.h"

static void *Vec2_ctor(void *_self, va_list *app)
{
    Vec3Class *self = _self;
    self->x = va_arg(*app, double);
    self->y = va_arg(*app, double);
    return self;
}

static void *Vec2_dtor(void *_self)
{
    Vec3Class *self = _self;
    self->x = 0;
    self->y = 0;
    free(_self);
    return self;
}

static void *Vec2_clone(const void *_self)
{
    const Vec2Class *self = _self;
    return new(Vec2, self->x, self->y);
}

static int Vec2_equal(const void *_self, const void *_b)
{
    const Vec3Class *self = _self;
    const Vec3Class *b = _b;
    if (self is b)
    {
        return 0;
    }
    if (not b or b->class isnt Vec2)
    {
        return 1;
    }
    return self->x is b->x and self->y is b->y;
}

static const Class vec2 = {
    sizeof(Vec2Class),
    Vec2_ctor,
    Vec2_dtor,
    Vec2_clone,
    Vec2_equal
};

const void * Vec2 = &vec2;
