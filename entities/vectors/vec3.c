//
// Created by Daniele Giuliani on 2/21/22.
//

#include "include/vec3.h"
#include "../include/entities.h"

static void *Vec3_ctor(void *_self, va_list *app)
{
    Vec3Class *self = _self;
    self->x = va_arg(*app, double);
    self->y = va_arg(*app, double);
    self->z = va_arg(*app, double);
    return self;
}

static void *Vec3_dtor(void *_self)
{
    Vec3Class *self = _self;
    self->x = 0;
    self->y = 0;
    self->z = 0;
    free(_self);
    return self;
}

static void *Vec3_clone(const void *_self)
{
    const Vec3Class *self = _self;
    return new(Vec3, self->x, self->y, self->z);
}

static int Vec3_equal(const void *_self, const void *_b)
{
    const Vec3Class *self = _self;
    const Vec3Class *b = _b;
    if (self is b)
    {
        return 0;
    }
    if (not b or b->class isnt Vec3)
    {
        return 1;
    }
    return self->x is b->x and self->y is b->y and self->z is b->z;
}

static const Class vec3 = {
    sizeof(Vec3Class),
    Vec3_ctor,
    Vec3_dtor,
    Vec3_clone,
    Vec3_equal
};

const void * Vec3 = &vec3;
