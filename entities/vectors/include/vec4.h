//
// Created by Daniele Giuliani on 2/21/22.
//

#ifndef DISTROINFOS_VEC4_H
#define DISTROINFOS_VEC4_H

#ifdef VEC4_IMPORT
#define EXTERN
#else
#define EXTERN extern
#endif

typedef struct VEC4 Vec4Class;

struct VEC4 {
    const void * class;	/* must be first */
    double x;
    double y;
    double z;
    double w;
};

EXTERN const void * Vec4;

#undef VEC4_IMPORT
#undef EXTERN

#endif //DISTROINFOS_VEC4_H
