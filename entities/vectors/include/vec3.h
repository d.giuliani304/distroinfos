//
// Created by Daniele Giuliani on 2/21/22.
//

#ifndef DISTROINFOS_VEC3_H
#define DISTROINFOS_VEC3_H

#ifdef VEC3_IMPORT
#define EXTERN
#else
#define EXTERN extern
#endif

typedef struct VEC3 Vec3Class;

struct VEC3 {
    const void * class;	/* must be first */
    double x;
    double y;
    double z;
};

EXTERN const void * Vec3;

#undef VEC3_IMPORT
#undef EXTERN

#endif //DISTROINFOS_VEC3_H
