//
// Created by Daniele Giuliani on 2/21/22.
//

#ifndef DISTROINFOS_VECTORS_H
#define DISTROINFOS_VECTORS_H

#include "vec2.h"
#include "vec3.h"
#include "vec4.h"

#ifdef VEC_IMPORT
#define EXTERN
#else
#define EXTERN extern
#endif

typedef struct VEC VecClass;
typedef struct VECLIST VecListClass;

struct VECLIST {
    const void * class;	/* must be first */
    int size;

    VecClass *start;
    VecClass *end;
};

struct VEC {
    const void * class;	/* must be first */
    void *element;
    void *next;
};

EXTERN const void * VecList;
EXTERN const void * Vec;

#undef VEC_IMPORT
#undef EXTERN

#endif //DISTROINFOS_VECTORS_H
