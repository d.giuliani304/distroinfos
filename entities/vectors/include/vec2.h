//
// Created by Daniele Giuliani on 2/21/22.
//

#ifndef DISTROINFOS_VEC2_H
#define DISTROINFOS_VEC2_H

#ifdef VEC2_IMPORT
#define EXTERN
#else
#define EXTERN extern
#endif

typedef struct VEC2 Vec2Class;

struct VEC2 {
    const void * class;	/* must be first */
    double x;
    double y;
};

EXTERN const void * Vec2;

#undef VEC2_IMPORT
#undef EXTERN

#endif //DISTROINFOS_VEC2_H
