//
// Created by Daniele Giuliani on 2/21/22.
//

#include "include/vectors.h"
#include "../include/entities.h"

static void *Vec4_ctor(void *_self, va_list *app)
{
    Vec4Class *self = _self;
    self->x = va_arg(*app, double);
    self->y = va_arg(*app, double);
    self->z = va_arg(*app, double);
    self->w = va_arg(*app, double);
    return self;
}

static void *Vec4_dtor(void *_self)
{
    Vec4Class *self = _self;
    self->x = 0;
    self->y = 0;
    self->z = 0;
    self->w = 0;
    free(_self);
    return self;
}

static void *Vec4_clone(const void *_self)
{
    const Vec4Class *self = _self;
    return new(Vec4, self->x, self->y, self->z, self->w);
}

static int Vec4_equal(const void *_self, const void *_b)
{
    const Vec4Class *self = _self;
    const Vec4Class *b = _b;
    if (self is b)
    {
        return 0;
    }
    if (not b or b->class isnt Vec4)
    {
        return 1;
    }
    return self->x is b->x and self->y is b->y and self->z is b->z and self->w is b->w;
}

static const Class vec4 = {
    sizeof(Vec4Class),
    Vec4_ctor,
    Vec4_dtor,
    Vec4_clone,
    Vec4_equal
};

const void * Vec4 = &vec4;
