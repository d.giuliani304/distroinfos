//
// Created by Daniele Giuliani on 2/21/22.
//

#ifndef DISTROINFOS_STRING_H
#define DISTROINFOS_STRING_H

#ifdef STRING_IMPORT
#define EXTERN
#else
#define EXTERN extern
#endif

typedef struct STRING StringClass;

struct STRING {
    const void *class;	/* must be first */
    char *text;
};

/**
 * @brief create a new text string passing an argument "text"
 * @param String text
 * @example StringClass *a = new(String, "input_names");\n
 * printf("%s", a->text);\n
 * delete(a);
 */
EXTERN const void * String;

#undef STRING_IMPORT
#undef EXTERN
#endif //DISTROINFOS_STRING_H
