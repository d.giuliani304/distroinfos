//
// Created by Daniele Giuliani on 2/21/22.
//
#include "include/string.h"
#include "../include/entities.h"


static void *String_ctor(void *_self, va_list *app)
{
    StringClass *self = _self;
    const char *text = va_arg(*app, const char *);

    self->text = malloc(strlen(text) + 1);
    assert(self->text);
    strcpy(self->text, text);
    return self;
}

static void *String_dtor(void *_self)
{
    StringClass *self = _self;
    free(self->text);
    free(_self);
    return self;
}

static void *String_clone(const void *_self)
{
    const StringClass *self = _self;
    return new(String, self->text);
}

static int String_equal(const void *_self, const void *_b)
{
    const StringClass *self = _self;
    const StringClass *b = _b;
    if (self is b)
        return 0;
    if (not b or b->class isnt String)
        return 1;
    return strcmp(self->text, b->text);
}

static const Class string = {
    sizeof(StringClass),
    String_ctor,
    String_dtor,
    String_clone,
    String_equal
};

const void * String = &string;
