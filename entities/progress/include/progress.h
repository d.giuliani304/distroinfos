//
// Created by Daniele Giuliani on 6/19/22.
//

#ifndef DISTROINFOS_PROGRESS_H
#define DISTROINFOS_PROGRESS_H

#include "../../window/include/window.h"
#include "../../vectors/include/vec3.h"
#include "../../vectors/include/vec2.h"
#include "../../../common/include/common.h"

#ifdef DISTROINFOS_PROGRESS_IMPORT
#define EXTERN
#else
#define EXTERN extern
#endif

typedef struct PROGRESS ProgressClass;

struct PROGRESS {
    const void *class;    /* must be first */
    WindowClass *target_window;
    int has_progress;
    float progress;
    float opacity;
    Vec3Class *color;
    Vec2Class *position;
    Vec2Class *size;
    Vec2Class *margin;
    SDL_Rect rect;
    ProgressClass *prev;
    ProgressClass *next;
    int (*setup) (ProgressClass *self, char *filename);
    void (*draw) (ProgressClass *self);

};

/**
 * @brief create a Progresss passing :arguments
 * @param :type :name
 * @example
 * <pre>\n &#9;</pre>
 */
EXTERN const void *Progress;

#undef DISTROINFOS_PROGRESS_IMPORT
#undef EXTERN
#endif //DISTROINFOS_PROGRESS_H
