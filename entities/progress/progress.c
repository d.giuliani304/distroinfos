#include "include/progress.h"
#include "../include/entities.h"

/* return 1 if the monitor supports full hd, 0 otherwise */
int construct_progress_bar(ProgressClass *self, const char * const progress_bar_json) {

    const cJSON *bars = NULL;
    const cJSON *bar = NULL;
    int status = 0;

    cJSON *progress_bar = cJSON_Parse(progress_bar_json);
    if (progress_bar == NULL)
    {
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_ptr);
        }
        printf("ERROR CJSON");
        status = 0;
        goto end;
    }

    bars = cJSON_GetObjectItemCaseSensitive(progress_bar, "bars");
    if (cJSON_IsArray(bars)) {
        printf("Parsing progress bar \n");
        ProgressClass *current_progress_bar = self;
        cJSON_ArrayForEach(bar, bars) {
            cJSON *src = cJSON_GetObjectItemCaseSensitive(bar, "src");
            // IGNORE SRC FOR NOW
            cJSON *color = cJSON_GetObjectItemCaseSensitive(bar, "color");
            if (!cJSON_IsObject(color)) {
                Debug printf("\"color\" progress bar not defined \n");
                status = 0;
                goto end;
            }
            cJSON *r = cJSON_GetObjectItemCaseSensitive(color, "r");
            cJSON *g = cJSON_GetObjectItemCaseSensitive(color, "g");
            cJSON *b = cJSON_GetObjectItemCaseSensitive(color, "b");
            if (not cJSON_IsNumber(r) or not cJSON_IsNumber(g) or not cJSON_IsNumber(b)) {
                Debug printf("\"color\" progress bar type mismatch \n");
                status = 0;
                goto end;
            }
            current_progress_bar->color =  new(Vec3, r->valueint*0.005, g->valueint*0.005, b->valueint*0.005);

            cJSON *opacity = cJSON_GetObjectItemCaseSensitive(bar, "opacity");
            if (not cJSON_IsNumber(opacity) and not cJSON_IsNull(opacity)) {
                Debug printf("\"opacity\" progress bar type mismatch \n");
                status = 0;
                goto end;
            }
            current_progress_bar->opacity = opacity->valuedouble;

            cJSON *position = cJSON_GetObjectItemCaseSensitive(bar, "position");
            if (!cJSON_IsObject(position)) {
                Debug printf("\"position\" progress bar not defined \n");
                status = 0;
                goto end;
            }
            cJSON *x = cJSON_GetObjectItemCaseSensitive(position, "x");
            cJSON *y = cJSON_GetObjectItemCaseSensitive(position, "y");
            if (cJSON_IsNull(x) or cJSON_IsNull(y)) {
                Debug printf("\"position\" progress bar cannot be NULL \n");
                status = 0;
                goto end;
            }
            // check if px
            current_progress_bar->position = new(Vec2, 0, 0);
            if (cJSON_IsString(x) and strpbrk(x->valuestring, "px") != NULL) {
                current_progress_bar->position->x = parse_numbers(x->valuestring);
            }
            //if % calculate the value based on current_progress_bar->target_window
            if (cJSON_IsString(x) and strpbrk(x->valuestring, "%") != NULL) {
                float value = parse_numbers(x->valuestring) * 0.01;
                current_progress_bar->position->x = current_progress_bar->target_window->surface->w * value;
            }
            if (cJSON_IsString(y) and strpbrk(y->valuestring, "px") != NULL) {
                current_progress_bar->position->y = parse_numbers(y->valuestring);
            }
            //if % calculate the value based on current_progress_bar->target_window
            if (cJSON_IsString(y) and strpbrk(y->valuestring, "%") != NULL) {
                float value = parse_numbers(y->valuestring) * 0.01;
                current_progress_bar->position->y = current_progress_bar->target_window->surface->h * value;
            }

            cJSON *size = cJSON_GetObjectItemCaseSensitive(bar, "size");
            if (!cJSON_IsObject(size)) {
                Debug printf("\"size\" progress bar not defined \n");
                status = 0;
                goto end;
            }
            cJSON *w = cJSON_GetObjectItemCaseSensitive(size, "w");
            cJSON *h = cJSON_GetObjectItemCaseSensitive(size, "h");
            if (cJSON_IsNull(w) or cJSON_IsNull(h)) {
                Debug printf("\"size\" progress bar type mismatch \n");
                status = 0;
                goto end;
            }
            // check if px
            current_progress_bar->size = new(Vec2, 0, 0);
            if (cJSON_IsString(w) and strpbrk(w->valuestring, "px") != NULL) {
                current_progress_bar->size->x = parse_numbers(w->valuestring);
            }
            //if % calculate the value based on current_progress_bar->target_window
            if (cJSON_IsString(w) and strpbrk(w->valuestring, "%") != NULL) {
                float value = parse_numbers(w->valuestring) * 0.01;
                current_progress_bar->size->x = current_progress_bar->target_window->surface->w * value;
            }
            if (cJSON_IsString(h) and strpbrk(h->valuestring, "px") != NULL) {
                current_progress_bar->size->y = parse_numbers(h->valuestring);
            }
            //if % calculate the value based on current_progress_bar->target_window
            if (cJSON_IsString(h) and strpbrk(h->valuestring, "%") != NULL) {
                float value = parse_numbers(h->valuestring) * 0.01;
                current_progress_bar->size->y = current_progress_bar->target_window->surface->h * value;
            }

            cJSON *has_progress = cJSON_GetObjectItemCaseSensitive(bar, "has_progress");
            if (!cJSON_IsNumber(has_progress)) {
                Debug printf("\"has_progress\" progress value mismatch \n");
                status = 0;
                goto end;
            }
            current_progress_bar->has_progress = has_progress->valueint;

            cJSON *margin = cJSON_GetObjectItemCaseSensitive(bar, "margin");
            if (!cJSON_IsObject(margin)) {
                Debug printf("\"margin\" progress value mismatch \n");
                status = 0;
                goto end;
            }
            cJSON *m_x = cJSON_GetObjectItemCaseSensitive(margin, "x");
            cJSON *m_y = cJSON_GetObjectItemCaseSensitive(margin, "y");
            if (cJSON_IsNull(m_x) or cJSON_IsNull(m_y)) {
                Debug printf("\"margin\" progress bar type mismatch \n");
                status = 0;
                goto end;
            }
            // check if px
            current_progress_bar->margin = new(Vec2, 0, 0);
            if (cJSON_IsString(m_x) and strpbrk(m_x->valuestring, "px") != NULL) {
                current_progress_bar->margin->x = parse_numbers(m_x->valuestring);
            }
            //if % calculate the value based on current_progress_bar->target_window
            if (cJSON_IsString(m_x) and strpbrk(m_x->valuestring, "%") != NULL) {
                float value = parse_numbers(m_x->valuestring) * 0.01;
                //TODO VEDERE MEGLIO
                current_progress_bar->margin->x = current_progress_bar->target_window->surface->w * value;
            }
            if (cJSON_IsString(m_y) and strpbrk(m_y->valuestring, "px") != NULL) {
                current_progress_bar->margin->y = parse_numbers(m_y->valuestring);
                current_progress_bar->position->y += current_progress_bar->margin->y;
            }
            //if % calculate the value based on current_progress_bar->target_window
            if (cJSON_IsString(m_y) and strpbrk(m_y->valuestring, "%") != NULL) {
                float value = parse_numbers(m_y->valuestring) * 0.01;
                current_progress_bar->margin->y = current_progress_bar->target_window->surface->h * value;
            }


            current_progress_bar->rect.x = current_progress_bar->position->x;
            current_progress_bar->rect.y = current_progress_bar->position->y;
            current_progress_bar->rect.w = current_progress_bar->size->x;
            current_progress_bar->rect.h = current_progress_bar->size->y;
            if(bar->next){
                current_progress_bar->next = new(Progress, current_progress_bar->target_window);
                current_progress_bar->next->prev = current_progress_bar;
                current_progress_bar = current_progress_bar->next;
            }else{
                current_progress_bar->next = NULL;
            }
        }
        status = 1;
        goto end;
    }
    end:
        cJSON_Delete(progress_bar);
        return status;
}

int setup(ProgressClass *self, char *filename) {
    char *fp = read_file(filename);
    if (not construct_progress_bar(self, fp)){
        Debug printf("Cannot setup progress bar with \"%s\"\n", filename);
        return 0;
    }
    return 1;
}

int ftou8(float value) {
    return fmax(0, fmin(255, (int)floor(value * 256.0)));
}

void draw(ProgressClass *self) {
    float progress = self->progress;
    do{
        if (self->has_progress){
            self->rect.w = self->size->x * progress;
        }
        SDL_SetRenderDrawColor(
            self->target_window->renderer,
            ftou8(self->color->x),
            ftou8(self->color->y),
            ftou8(self->color->z),
            ftou8(self->opacity)
        );
        if (self->opacity < 1) {
            SDL_SetRenderDrawBlendMode(self->target_window->renderer, SDL_BLENDMODE_BLEND);
        }
        SDL_RenderFillRect(self->target_window->renderer, &self->rect);
        self = self->next;
    }
    while (self);
}

static void *Progress_ctor(void *_self, va_list *app) {
    ProgressClass *self = _self;
    self->target_window = va_arg(*app, WindowClass*);
    assert(self->target_window);
    self->progress = 1.0;
    self->opacity = 1.0;
    self->color = new(Vec3, 0,0,0);
    assert(self->color);
    self->next = NULL;
    self->prev = NULL;
    self->setup = setup;
    self->draw = draw;
    return self;
}

static void *Progress_dtor(void *_self) {
    ProgressClass *self = _self;
    self->progress = 0.0;
    self->opacity = 1.0;
    delete(self->color);
    if (self->next) {
        delete(self->next);
    }
    self->target_window->prepareRender(self->target_window);
    self->target_window->render(self->target_window);
    free(_self);
    return self;
}

static void *Progress_clone(const void *_self) {
    const ProgressClass *self = _self;
    return new(Progress, self->target_window);
}

static int Progress_equal(const void *_self, const void *_b) {
    const ProgressClass *self = _self;
    const ProgressClass *b = _b;
    if (self is b)
    {
        return 0;
    }
    if (not b or b->class isnt Progress) {
        return 1;
    }
    return self->target_window is b->target_window;
}

static const Class progress = {
    sizeof(ProgressClass),
    Progress_ctor,
    Progress_dtor,
    Progress_clone,
    Progress_equal
};

const void *Progress = &progress;