//
// Created by Daniele Giuliani on 2/21/22.
//

#ifndef DISTROINFOS_ENTITIES_H
#define DISTROINFOS_ENTITIES_H

#include <assert.h>

#include "../../include/debug.h"

#include "../string/include/string.h"
#include "../uuid/include/uuid.h"
#include "../shape/include/shape.h"
#include "../point/include/points.h"
#include "../window/include/window.h"
#include "../progress/include/progress.h"

#define is ==
#define isnt !=
#define not !
#define and &&
#define or ||
#define in ,

#ifdef ENTITIES_IMPORT
#define EXTERN
#else
#define EXTERN extern
#endif

typedef struct CLASS Class;

struct CLASS {
    size_t size;
    void * (* ctor) (void *self, va_list *app);
    void * (* dtor) (void *self);
    void * (* clone) (const void *self);
    int (* differ) (const void *self, const void *b);
};

void *new(const void *_class, ...);
void delete(void *item);

void *clone(const void * self);
int differ(const void * self, const void * b);

size_t sizeOf(const void * self);


/* FUNCTION PROTOTYPES */

#undef ENTITIES_IMPORT
#undef EXTERN

#endif //DISTROINFOS_ENTITIES_H
