//
// Created by Daniele Giuliani on 2/21/22.
//
#include "include/point2.h"
#include "../include/entities.h"

Vec2Class *Point2_Position(Point2Class * self, double x, double y) {
    self->position->x = x;
    self->position->y = y;
    return self->position;
}

static void *Point2_ctor(void *_self, va_list *app)
{
    Point2Class *self =_self;
    self->position = va_arg(*app, Vec2Class *);
    assert(self->position);
    self->setPosition = Point2_Position;
    return self;
}

static void *Point2_dtor(void *_self)
{
    Point2Class *self = _self;
    delete(self->position);
    free(_self);
    return self;
}

static void *Point2_clone(const void *_self)
{
    const Point2Class *self = _self;
    return new(Point2, self->position);
}

static int Point2_equal(const void *_self, const void *_b)
{
    const Point2Class *self = _self;
    const Point2Class *b = _b;
    if (self is b)
    {
        return 0;
    }
    if (not b or b->class isnt Point2)
    {
        return 1;
    }
    return self->position is b->position;
}

static const Class point2 = {
    sizeof(Point2Class),
    Point2_ctor,
    Point2_dtor,
    Point2_clone,
    Point2_equal
};

const void * Point2 = &point2;
