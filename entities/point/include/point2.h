//
// Created by Daniele Giuliani on 2/21/22.
//

#ifndef DISTROINFOS_POINT2_H
#define DISTROINFOS_POINT2_H

#include "../../vectors/include/vec2.h"

#ifdef POINT2_IMPORT
#define EXTERN
#else
#define EXTERN extern
#endif

typedef struct POINT2 Point2Class;

struct POINT2 {
    const void *class;	/* must be first */
    Vec2Class *position;
    Vec2Class *(*setPosition) (Point2Class* self, double x, double y);
};

EXTERN const void * Point2;

#undef POINT2_IMPORT
#undef EXTERN

#endif //DISTROINFOS_POINT2_H
