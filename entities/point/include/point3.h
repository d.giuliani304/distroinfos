//
// Created by Daniele Giuliani on 2/21/22.
//

#ifndef DISTROINFOS_POINT3_H
#define DISTROINFOS_POINT3_H

#include "../../vectors/include/vec3.h"

#ifdef POINT3_IMPORT
#define EXTERN
#else
#define EXTERN extern
#endif

typedef struct POINT3 Point3Class;

struct POINT3 {
    const void *class;	/* must be first */
    Vec3Class *position;
    Vec3Class *(*setPosition) (Point3Class* self, double x, double y, double z);
};

EXTERN const void * Point3;

#undef POINT3_IMPORT
#undef EXTERN

#endif //DISTROINFOS_POINT3_H
