//
// Created by Daniele Giuliani on 2/21/22.
//
#include "include/point3.h"
#include "../include/entities.h"

Vec3Class *Point3_Position(Point3Class * self, double x, double y, double z) {
    self->position->x = x;
    self->position->y = y;
    self->position->z = z;
    return self->position;
}

static void *Point3_ctor(void *_self, va_list *app)
{
    Point3Class *self =_self;
    self->position = va_arg(*app, Vec3Class *);
    assert(self->position);
    self->setPosition = Point3_Position;
    return self;
}

static void *Point3_dtor(void *_self)
{
    Point3Class *self = _self;
    delete(self->position);
    free(_self);
    return self;
}

static void *Point3_clone(const void *_self)
{
    const Point3Class *self = _self;
    return new(Point3, self->position);
}

static int Point3_equal(const void *_self, const void *_b)
{
    const Point3Class *self = _self;
    const Point3Class *b = _b;
    if (self is b)
    {
        return 0;
    }
    if (not b or b->class isnt Point3)
    {
        return 1;
    }
    return self->position is b->position;
}

static const Class point3 = {
    sizeof(Point3Class),
    Point3_ctor,
    Point3_dtor,
    Point3_clone,
    Point3_equal
};

const void * Point3 = &point3;
