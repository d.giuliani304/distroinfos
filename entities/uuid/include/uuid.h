//
// Created by Daniele Giuliani on 6/13/22.
//

#ifndef DISTROINFOS_UUID_H
#define DISTROINFOS_UUID_H

#include <uuid/uuid.h>

#ifdef DISTROINFOS_UUID_IMPORT
#define EXTERN
#else
#define EXTERN extern
#endif

typedef struct UUID UuidClass;
struct UUID {
    const void *class;	/* must be first */
    uuid_t  binuuid;
    char    uuid[UUID_STR_LEN];
};
EXTERN const void * Uuid;

#undef DISTROINFOS_UUID_IMPORT
#undef EXTERN
#endif //DISTROINFOS_UUID_H
