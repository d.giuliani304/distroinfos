//
// Created by Daniele Giuliani on 6/13/22.
//

#include "include/uuid.h"
#include "../include/entities.h"

static void *Uuid_ctor(void *_self, va_list *app)
{
    UuidClass *self =_self;
    uuid_generate_random(self->binuuid);
    assert(self->binuuid);
    uuid_unparse_upper(self->binuuid, self->uuid);
    assert(self->uuid);
    return self;
}

static void *Uuid_dtor(void *_self)
{
    UuidClass *self = _self;
    free(_self);
    return self;
}

static void *Uuid_clone(const void *_self)
{
    return new(Uuid);
}

static int Uuid_equal(const void *_self, const void *_b)
{
    const UuidClass *self = _self;
    const UuidClass *b = _b;
    if (self is b)
    {
        return 0;
    }
    if (not b or b->class isnt Uuid)
    {
        return 1;
    }
    return 0;
}

static const Class uuid = {
    sizeof(UuidClass),
    Uuid_ctor,
    Uuid_dtor,
    Uuid_clone,
    Uuid_equal
};

const void * Uuid = &uuid;
