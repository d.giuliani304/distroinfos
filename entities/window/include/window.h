//
// Created by Daniele Giuliani on 6/13/22.
//

#ifndef DISTROINFOS_WINDOW_H
#define DISTROINFOS_WINDOW_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "../../string/include/string.h"
#include "../../uuid/include/uuid.h"

#ifdef DISTROINFOS_WINDOW_IMPORT
#define EXTERN
#else
#define EXTERN extern
#endif
typedef enum WindowPositionsEnum {
    CENTER = SDL_WINDOWPOS_CENTERED,
    UNDEFINED = SDL_WINDOWPOS_UNDEFINED
} WindowPositions;

typedef enum WindowTypesEnum {
    SPLASH = SDL_WINDOW_BORDERLESS|SDL_WINDOW_OPENGL,
    MAIN = SDL_WINDOW_VULKAN
} WindowTypes;

typedef struct WINDOW WindowClass;

struct WINDOW {
    const void *class;	/* must be first */
    int splash;
    int is_main_window;
    StringClass *name;
    UuidClass *uuid;
    SDL_Window *window;
    SDL_Surface *surface;
    /*---------THERE'S NOTHING BELOW---------*/
    /*---*/  SDL_Renderer *renderer;    /*---*/
    /*---*/  SDL_Texture *texture;      /*---*/
    /*---------THERE'S NOTHING ABOVE---------*/
    void (*prepareRender) (WindowClass *window);
    void (*render) (WindowClass *window);
};

/**
 * @brief create a Windows passing a String Name, WindowPosition Position,
 * int Width, int Height, WindowTypes Flags
 * @param String Name
 * @param WindowPositions Position
 * @param int Width
 * @param int Height
 * @param WindowTypes Flags
 * @example
 * <pre>WindowClass *w = new(\n &#9; Window,\n &#9; "window name",\n &#9; CENTER,\n &#9; 800,\n &#9; 600,\n &#9; SPLASH\n);</pre>
 */
EXTERN const void * Window;

#undef DISTROINFOS_WINDOW_IMPORT
#undef EXTERN

#endif //DISTROINFOS_WINDOW_H
