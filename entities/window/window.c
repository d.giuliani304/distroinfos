//
// Created by Daniele Giuliani on 6/13/22.
//

#include "include/window.h"
#include "../../entities/include/entities.h"

static void splashWindowSetup(WindowClass *self) {
    SDL_Surface *temp_surface = IMG_Load("medias/BG.webp");
    if (temp_surface is NULL){
        printf("Could not create temp surface: %s\n", SDL_GetError());
    }
    self->surface = SDL_CreateRGBSurface(0, 800, 480, temp_surface->format->BitsPerPixel, 0,0,0,0);
    if (self->surface is NULL){
        printf("Could not create surface: %s\n", SDL_GetError());
    }
    if (SDL_BlitScaled(temp_surface, NULL, self->surface, NULL) != 0){
        printf("Could not create scaled surface: %s\n", SDL_GetError());
    }

}

static void createTextureFromSurface(WindowClass *self) {
    self->renderer = SDL_CreateRenderer(self->window, -1, SDL_RENDERER_ACCELERATED);
    if (self->renderer is NULL){
        printf("Could not create renderer: %s\n", SDL_GetError());
    }
    self->texture = SDL_CreateTextureFromSurface(self->renderer, self->surface);
    if (self->texture is NULL){
        printf("Could not create texture: %s\n", SDL_GetError());
    }
}

void createAppIcon(SDL_Window *window)
{
    SDL_Surface *surface = IMG_Load("medias/icon.webp");
    SDL_SetWindowIcon(window, surface);
    SDL_FreeSurface(surface);
}

SDL_Window *createWindow(va_list *app, WindowClass *self) {
    SDL_Window *window;
    int pos_x = va_arg(*app, WindowPositions);
    int pos_y = pos_x;
    int width = va_arg(*app, int);
    int height = va_arg(*app, int);
    Uint32 flags_ = va_arg(*app, WindowPositions);
    self->splash = 1;
    self->is_main_window = 0;
    if (flags_ is SPLASH)
    {
        splashWindowSetup(self);
        width = self->surface->w;
        height = self->surface->h;
        self->splash = 0;
        self->is_main_window = 1;
    }
    // else if(flags_ is MAIN){
    // mainWindowSetup();
    // }
    SDL_Init(SDL_INIT_VIDEO);
    self->window = SDL_CreateWindow(
        self->name->text,
        pos_x,
        pos_y,
        width,
        height,
        flags_
    );
    createAppIcon(self->window);
    if (self->window is NULL)
    {
        printf("Could not create window: %s\n", SDL_GetError());
    }
    if (flags_ is SPLASH)
    {
        createTextureFromSurface(self);
    }
    return window;
}

void prepareRender(WindowClass *self)
{
    SDL_RenderClear(self->renderer);
    SDL_RenderCopy(
    self->renderer,
    self->texture,
    NULL,
    NULL
    );
}

void render(WindowClass *self)
{
    SDL_RenderPresent(self->renderer);
}

static void *Window_ctor(void *_self, va_list *app)
{
    WindowClass *self = _self;
    self->name = new(String, va_arg(*app, const char *));
    self->uuid = new(Uuid);
    createWindow(app, self);
    self->prepareRender = prepareRender;
    self->render = render;

    return self;
}

static void *Window_dtor(void *_self)
{
    WindowClass *self = _self;
    delete(self->name);
    delete(self->uuid);
    if (self->splash)
    {
        SDL_DestroyTexture(self->texture);
        SDL_DestroyRenderer(self->renderer);
    }
    SDL_DestroyWindow(self->window);
    if (self->splash)
    {
        IMG_Quit();
    }
    if (self->is_main_window)
    {
        SDL_Quit();
    }
    free(_self);
    return self;
}

//TODO CLONE WINDOW
static void *Window_clone(const void *_self)
{
    const WindowClass *self = _self;
    return new(Window, self->name);
}

static int Window_equal(const void *_self, const void *_b)
{
    const WindowClass *self = _self;
    const WindowClass *b = _b;
    if (self is b)
    {
        return 0;
    }
    if (not b or b->class isnt Window)
    {
        return 1;
    }
    return (self->name is b->name) or (self->uuid->binuuid is b->uuid->binuuid);
}

static const Class window = {
    sizeof(WindowClass),
    Window_ctor,
    Window_dtor,
    Window_clone,
    Window_equal
};

const void * Window = &window;